FROM node:12-alpine

RUN mkdir /src
WORKDIR /src

COPY . /src/

RUN npm install --verbose
